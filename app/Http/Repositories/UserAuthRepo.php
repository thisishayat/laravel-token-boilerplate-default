<?php
/**
 * Created by PhpStorm.
 * User: backend
 * Date: 2/19/18
 * Time: 4:12 PM
 */
namespace App\Http\Repositories;


use App\Country;
use App\EmailTokenDtl;
use App\Helper\Helper;
use App\MobileSignupTokens;
use App\PhoneCodeDetails;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserAuthRepo
{

    /**
     *
     * @param $postVars
     * @return array|int|string
     */

    public function webUserSignp($request)
    {

        $status = [
            'status' => trans('custom.status.validError'),
            'msg' => trans('auth.errors.dataInsertFail')
        ];
        $params = $request->all();
        $validator = Validator::make($params, [
            'username' => 'required|numeric',
            'name' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator != '' && $validator->fails()) {
            $errors = $validator->errors()->toArray();
            $errorArr = array_flatten($errors);
            $status = [
                'status' => trans('custom.status.validError'),
                'msg' => $errorArr[0],
                'error' => $errors

            ];
            return $status;
        } else {
            $params['password'] = bcrypt($params['password']);
            $user = User::create([
                'username' => $params['username'],
                'password' => $params['password'],
                'name' => $params['name'],
            ]);

            DB::commit();
            dd('success');
            $status = [
                'status' => trans('custom.status.success'),
                'msg' => trans('custom.msg.signUpSuccess')
            ];


            return $status;

        }
    }

    public function login($request)
    {
        try {
            $input = $request->input();
            $validator = Validator::make($request->all(), [
                'username' => 'required|string',
                'password' => 'required|string',
            ]);
            if ($validator->fails()) {
                return ['status' => 5000, 'error' => $validator->errors()];
            }
            $credentials = array(
                'username' => $input['username'],
                'password' => $input['password'],

            );
            $remember = isset($input['remember']) ? $input['remember'] : false;

            if (Auth::attempt($credentials, $remember)) {
                $res = [
                    'status'=>trans('custom.status.success'),
                    'msg'=>trans('custom.msg.dataGet'),
                    'data' => Auth::user()->toArray(),
                ];
            } else {
                $res = [
                    'status'=>trans('custom.status.failed'),
                    'msg'=>trans('custom.msg.invalid'),
                    'login not success'=>'Login not success',
                ];
            }
        } catch (Exception $e) {
            $res = [
                'status'=>trans('custom.status.failed'),
                'msg'=>trans('custom.msg.invalid'),
                'error' => $e->getCode()
            ];
        }
        return $res;

    }

    public function getUserData($request)
    {
        dd(Auth::user()->name);

    }





}