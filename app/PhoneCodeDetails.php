<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneCodeDetails extends Model
{
    protected $table = "phone_code_details";
    public $timestamps = false;
}
